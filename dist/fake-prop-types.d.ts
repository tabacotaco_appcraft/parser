export function _getPropDefinition(propTypes: any): {
    uid: string;
    type: string;
    required: boolean;
    options: {};
};
declare var _default: any;
export default _default;
