# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.0.1](https://gitlab.com/tabacotaco_appcraft/parser/compare/v1.1.5...v1.0.1) (2022-07-20)

### [1.1.5](https://gitlab.com/tabacotaco_appcraft/prop-types-parser/compare/v1.1.4...v1.1.5) (2022-07-20)

### 1.1.4 (2022-07-20)

### [1.3.5](https://git.gorilla-technology.com/gorilla/appcraft/prop-types-parser/compare/v1.3.4...v1.3.5) (2022-06-29)


### Others

* **versionrc:** update info ([d3d2658](https://git.gorilla-technology.com/gorilla/appcraft/prop-types-parser/commit/d3d2658b857c48bbab8f0e887c419c9fe3b40690))

### [1.3.4](https://git.gorilla-technology.com/gorilla/f2e/ciap-essentials/compare/v1.3.3...v1.3.4) (2022-04-06)


### Others

* add readme ([6bf4c26](https://git.gorilla-technology.com/gorilla/f2e/ciap-essentials/commit/6bf4c26cd56aa972ee6b45841839643fd39df9cf))

### [1.3.3](https://git.gorilla-technology.com/gorilla/f2e/ciap-essentials/compare/v1.3.2...v1.3.3) (2022-03-25)


### Bug Fixes

* **fake-prop-types:** bug fixed ([2ec2d9a](https://git.gorilla-technology.com/gorilla/f2e/ciap-essentials/commit/2ec2d9afdbf6c3b940029f7ed7157383cc0a0af2))

### [1.3.2](https://git.gorilla-technology.com/gorilla/f2e/ciap-essentials/compare/v1.3.1...v1.3.2) (2022-03-25)


### Bug Fixes

* **fake-prop-types:** fixed bugs ([a191122](https://git.gorilla-technology.com/gorilla/f2e/ciap-essentials/commit/a19112229032f03b19cb45b96f148755a945f31c))

### [1.3.1](https://git.gorilla-technology.com/gorilla/f2e/ciap-essentials/compare/v1.3.0...v1.3.1) (2022-03-25)


### Bug Fixes

* **fake-prop-types:** fixed the instanceOf type process ([e2abfd7](https://git.gorilla-technology.com/gorilla/f2e/ciap-essentials/commit/e2abfd73af7858d26c1a586dbe80f8a7b6d6a82e))

## [1.3.0](https://git.gorilla-technology.com/gorilla/f2e/ciap-essentials/compare/v1.2.1...v1.3.0) (2022-03-21)


### Features

* 支援轉換時，可自行指定 defaultType ([0b8bbdd](https://git.gorilla-technology.com/gorilla/f2e/ciap-essentials/commit/0b8bbdd879f43ab7ea97d5c730d1f33a8a882f5f))


### Others

* reverse ([ceff109](https://git.gorilla-technology.com/gorilla/f2e/ciap-essentials/commit/ceff10903bdf149541388feb5b127881c76aff63))

### [1.2.1](https://git.gorilla-technology.com/gorilla/f2e/ciap-essentials/compare/v1.2.0...v1.2.1) (2022-03-18)


### Bug Fixes

* allowed target decoration's propTypes/configTypes is null ([b3095cd](https://git.gorilla-technology.com/gorilla/f2e/ciap-essentials/commit/b3095cd128d1c0c3bdcc2a7d42d5b0350c43ac9c))

## [1.2.0](https://git.gorilla-technology.com/gorilla/f2e/ciap-essentials/compare/v1.1.3...v1.2.0) (2022-03-18)


### Features

* surport to get hoc prop types ([eca0527](https://git.gorilla-technology.com/gorilla/f2e/ciap-essentials/commit/eca0527ea5aa15174bfc2371642825abad55c9c6))

### [1.1.3](https://git.gorilla-technology.com/gorilla/f2e/ciap-essentials/compare/v1.1.2...v1.1.3) (2022-03-15)


### Bug Fixes

* 修正 proptypes definition 的基本格式 ([67ff617](https://git.gorilla-technology.com/gorilla/f2e/ciap-essentials/commit/67ff6178c2d190f75da076eb3e2a17f961ba2b4b))

### [1.1.2](https://git.gorilla-technology.com/gorilla/f2e/ciap-essentials/compare/v1.1.1...v1.1.2) (2022-03-11)


### Bug Fixes

* sandbox add require function ([d3cf669](https://git.gorilla-technology.com/gorilla/f2e/ciap-essentials/commit/d3cf66905dd16c3941db4c6f1d28ef4953a01d92))

### [1.1.1](https://git.gorilla-technology.com/gorilla/f2e/ciap-essentials/compare/v1.1.0...v1.1.1) (2022-02-25)


### Others

* **npmignore:** add tsconfig ([804e238](https://git.gorilla-technology.com/gorilla/f2e/ciap-essentials/commit/804e238e6ad64744521c92375c5a67c3c6c439c2))

## [1.1.0](https://git.gorilla-technology.com/gorilla/f2e/ciap-essentials/compare/v1.0.7...v1.1.0) (2022-02-24)


### Features

* **sandbox:** add all properties of window into sandbox ([7ca3211](https://git.gorilla-technology.com/gorilla/f2e/ciap-essentials/commit/7ca3211beea412bc4a3d8dbe8b80fca9550ee2f1))

### [1.0.7](https://git.gorilla-technology.com/gorilla/f2e/ciap-essentials/compare/v1.0.6...v1.0.7) (2022-01-26)


### Bug Fixes

* **.d.ts:** add export new type: PropType ([aac960a](https://git.gorilla-technology.com/gorilla/f2e/ciap-essentials/commit/aac960aec9254f35aaa928f002c0b79dee180ef8))

### [1.0.6](https://git.gorilla-technology.com/gorilla/f2e/ciap-essentials/compare/v1.0.5...v1.0.6) (2021-11-30)


### Others

* fixed types ([9ec7db2](https://git.gorilla-technology.com/gorilla/f2e/ciap-essentials/commit/9ec7db260aada21cd99333f604b5cd992f4e5327))

### [1.0.5](https://git.gorilla-technology.com/gorilla/f2e/ciap-essentials/compare/v1.0.4...v1.0.5) (2021-11-30)


### Others

* fixed types ([ba9c26b](https://git.gorilla-technology.com/gorilla/f2e/ciap-essentials/commit/ba9c26b34b372de31455cd61d3346acc93604fb4))

### [1.0.4](https://git.gorilla-technology.com/gorilla/f2e/ciap-essentials/compare/v1.0.3...v1.0.4) (2021-11-30)


### Others

* fixed types ([f1a1292](https://git.gorilla-technology.com/gorilla/f2e/ciap-essentials/commit/f1a1292e4b50835d56290184123390f3f6fa9b39))

### [1.0.3](https://git.gorilla-technology.com/gorilla/f2e/ciap-essentials/compare/v1.0.2...v1.0.3) (2021-11-30)


### Others

* fixed types ([46e4871](https://git.gorilla-technology.com/gorilla/f2e/ciap-essentials/commit/46e487121a8fffe08af681a70c42f5ad8bd0478b))

### [1.0.2](https://git.gorilla-technology.com/gorilla/f2e/ciap-essentials/compare/v1.0.1...v1.0.2) (2021-11-29)


### Others

* fixed types ([b157da4](https://git.gorilla-technology.com/gorilla/f2e/ciap-essentials/commit/b157da497d05b2aa7f865097bbbfb0255ff54a77))

### [1.0.1](https://git.gorilla-technology.com/gorilla/f2e/ciap-essentials/compare/v0.0.7...v1.0.1) (2021-11-29)


### Bug Fixes

* **index:** 修正 PropDefinition ([503d811](https://git.gorilla-technology.com/gorilla/f2e/ciap-essentials/commit/503d8113880a60ba0b5f3d17d59768765eec24a0))

### [0.0.7](https://git.gorilla-technology.com/gorilla/f2e/ciap-essentials/compare/v0.0.6...v0.0.7) (2021-11-26)


### Others

* **.npmrc:** update config ([7085957](https://git.gorilla-technology.com/gorilla/f2e/ciap-essentials/commit/708595753d50d2f77a4aaa75110ab4e073bf09cf))
* **index:** 修正 export 的 type 內容 ([762efa5](https://git.gorilla-technology.com/gorilla/f2e/ciap-essentials/commit/762efa5b518266784b9f7d222fc8d58fc6991bc7))

### [0.0.6](https://git.gorilla-technology.com/gorilla/f2e/ciap-essentials/compare/v0.0.5...v0.0.6) (2021-11-26)


### Others

* **index:** add export def type ([214d655](https://git.gorilla-technology.com/gorilla/f2e/ciap-essentials/commit/214d655069befb09095a5e95ffd039a3f766a2af))

### [0.0.5](https://git.gorilla-technology.com/gorilla/f2e/ciap-essentials/compare/v0.0.4...v0.0.5) (2021-11-26)


### Others

* **index.ts:** 增加較詳細的 definition type ([b444f17](https://git.gorilla-technology.com/gorilla/f2e/ciap-essentials/commit/b444f178dd52be5c0255c7ebbed95e37b1b804c8))

### [0.0.4](https://git.gorilla-technology.com/gorilla/f2e/ciap-essentials/compare/v0.0.3...v0.0.4) (2021-11-19)


### Features

* add ts definition file ([4c0eb54](https://git.gorilla-technology.com/gorilla/f2e/ciap-essentials/commit/4c0eb541ec273bd5c61c9f52c1028af6a153918a))

### [0.0.3](https://git.gorilla-technology.com/gorilla/f2e/ciap-essentials/compare/v0.0.2...v0.0.3) (2021-11-15)


### Bug Fixes

* bugfixed ([be69eba](https://git.gorilla-technology.com/gorilla/f2e/ciap-essentials/commit/be69ebafbf65a030452e79f6216dfc5de4347b5e))

### 0.0.2 (2021-11-15)


### Features

* init commit ([4231510](https://git.gorilla-technology.com/gorilla/f2e/ciap-essentials/commit/4231510ddc671da71b184839c35507f6b48a3fc6))


### Others

* init project ([b767075](https://git.gorilla-technology.com/gorilla/f2e/ciap-essentials/commit/b767075e34c9beda084ffeafa2b5fe877b46fd98))
